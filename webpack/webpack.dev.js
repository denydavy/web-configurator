/* eslint-disable import/no-extraneous-dependencies */
const merge = require('webpack-merge');

const base = require('./webpack.base');

const dev = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          plugins: ['react-hot-loader/babel'],
        },
      },
    ],
  },
  devtool: 'eval-source-map',
  devServer: {
		proxy: [{
			target: 'http://localhost:8081',
			context: ['/v1'],
		}],
    port: 3000,
    historyApiFallback: true,
  },
	watchOptions: {
		aggregateTimeout: 3e2,
		poll: 3e3,
	}
};

module.exports = merge(base, dev);
