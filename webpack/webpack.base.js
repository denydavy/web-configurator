/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const SRC = path.resolve(process.cwd(), 'src');
const BUILD = path.resolve(process.cwd(), 'build');

module.exports = {
  entry: [
	  'whatwg-fetch',
	  path.resolve(SRC, 'index.jsx'),
  ],
  output: {
    path: BUILD,
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },
  module: {
    rules: [
      {
        test: /\.tsv/,
        loader: 'dsv-loader?delimiter=\t',
      },
    ],
  },
  optimization: {
    splitChunks: {
      name: process.env.NODE_ENV === 'development',
      cacheGroups: {
        default: false,
        common: {
          test: /node_modules/,
          enforce: true,
          name: 'vendor',
          chunks: 'all',
          reuseExistingChunk: true,
        },
      },
    },
  },
  plugins: [
    new CleanWebpackPlugin([BUILD]),
    new HtmlWebpackPlugin({
      template: path.resolve(SRC, 'index.html'),
    }),
  ],
  resolve: {
    modules: ['node_modules', SRC],
    extensions: ['.js', '.jsx'],
  },
  target: 'web',
};
