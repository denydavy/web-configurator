import {createReducer} from 'redux-act';
import set from 'lodash/fp/set';
import {initPosition, setUnit, setFactory} from './actions';

const initialState = {
	unit: null,
	pos: null,
	factory: null,
};

export default createReducer({
	[initPosition]: (state, {pos})  => set('pos', pos, state),
	[setUnit]: (state, {unit}) => set('unit', unit, state),
	[setFactory]: (state, {factory}) => set('factory', factory, state),
}, initialState);
