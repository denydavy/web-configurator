// @flow
import 'babel-polyfill';
import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import App from 'containers/App';
import LanguageProvider from 'containers/LanguageProvider';
import store from 'store';

const root = document.getElementById('app');

if (!root) {
  throw new Error('Can not find #app');
}

render(
	<Provider store={store}>
		<LanguageProvider>
			<Router>
				<Route path='/' component={App} />
			</Router>
		</LanguageProvider>
	</Provider>,
	root,
);
