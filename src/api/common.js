// @flow
import omitBy from 'lodash/fp/omitBy';
import isNil from 'lodash/fp/isNil';

export const omitNil = omitBy(isNil);
export const handleError = (error: Error) => ({error});
export const handleJson = (response: Response): Promise<Object> => response.json();
export const handleText = (response: Response): Promise<string> => response.text();
export function handleResponse(response: Response) {
	const contentType: string = response.headers.get('content-type') || '';

	switch(contentType) {
		case 'application/json':
			return handleJson(response);
		case 'plain/text':
			return handleText(response);
		default:
			return handleError(
				new Error(`Unknown response type: ${contentType}`)
			);
	}
}
const V1 = '/v1';
export const PFX_CFG = `${V1}/configurator`;
export const queryUrl = (url: string, query: Object = {}): URL => {
	const urlObj: URL = new URL(url, window.location);

  // Loop to map Object to URL.searchParams
	[...Object.entries(query)]
		.reduce(
			(u: URL, [k: string, v: mixed]) => u.searchParams.set(k, v),
			urlObj
		);

	return urlObj;
};
