// @flow
import {PFX_CFG, handleResponse, handleError, queryUrl, omitNil} from 'api/common';
import type {Changeset/* , Unit */} from 'containers/Units/types';

export const fetchUnits = ({parent}: {parent: Array<string> | void}): Promise<Object> =>
	fetch(queryUrl(`${PFX_CFG}`, omitNil({'unit_uids': parent})))
		.then(handleResponse, handleError)
		.then((units: Object) => ({units}));

export const postChanges = (changes: Object): Promise<Object> =>
	fetch(
		queryUrl(`${PFX_CFG}`),
		{method: 'POST', body: JSON.stringify(changes)}
	)
		.then(handleResponse, handleError)
		.then((changeset: Changeset) => ({changeset}));
