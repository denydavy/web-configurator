import {PFX_CFG, handleResponse, handleError, queryUrl, omitNil} from 'api/common';

export const getTemplates = () =>
	fetch(
		`${PFX_CFG}/templates`, {
			method: 'GET',
		}
	)
		.then(handleResponse, handleError);

export const postTmplChanges = (changes: Object): Promise<Object> =>
	fetch(
		`${PFX_CFG}/templates`,
		{
			method: 'POST',
			body: JSON.stringify(changes)
		}
	)
		.then(handleResponse, handleError)
		.then(changeset => ({changeset}));

export const postTmplAssign = (changes: Object): Promise<Object> =>
	fetch(
		`${PFX_CFG}/assignments`,
		{
			method: 'POST',
			body: JSON.stringify(changes)
		}
	)
		.then(handleResponse, handleError)
		.then(changeset => ({changeset}));

