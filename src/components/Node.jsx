// @flow

import * as React from 'react';
import {compose} from 'redux';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

import {withWorkbench} from 'containers/Workbench';

import type {Unit} from 'containers/Units/types';

const styles = {
	root: {
		display: 'flex',
		backgroundColor: grey[200],
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%',
		height: '100%',
	},
};

type Props = {
	unit: Unit,
	classes: Object,
	setUnit: Function,
};

class Node extends React.Component<Props> {
	putOnWorkbench = () => {
		const {setUnit, unit} = this.props;

		setUnit(unit.uid);
	}

	render() {
		const {unit: {display_id, status}, classes} = this.props;

		return (
			<div className={classes.root} onClick={this.putOnWorkbench}>
				<Typography variant='title'>
					{display_id}
				</Typography>
				<Typography variant='subheading'>
					{status}
				</Typography>
			</div>
		);
	}
}

export default compose(
	withStyles(styles),
	withWorkbench
)(Node);
