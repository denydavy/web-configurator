import React from 'react';
import * as prop from 'utils/props';

// TODO: collect multiple units
export default Component => class TemplateCollector extends React.Component {
	state = {
		template: {},
	}

	setTmplProp = (path, item) => this.setState(({template}) => ({
		template: prop.set(template, path, item),
	}));

	getTmplProp = (path, id) => prop.get(this.state.template, path, id);

	getTmpl = () => prop.toList(this.state.template);

	rmTmplProp = (path, id) => this.setState(({template}) => ({
		template: prop.rm(template, path, id),
	}));

	render() {
		const {state, props, ...methods} = this;

		return <Component {...state} {...props} {...methods} />;
	}
}

