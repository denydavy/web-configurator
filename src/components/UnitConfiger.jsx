import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl'
import filter from 'lodash/fp/filter';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import isEqual from 'lodash/fp/isEqual';
import sample from 'lodash/fp/sample';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import AddIcon from '@material-ui/icons/Add';
import {withStyles} from '@material-ui/core/styles';

import {unitType, withUnit} from 'containers/Units';
import UnitMaker, {withUnitCollector} from 'components/UnitMaker';
import withTmplCollector from 'components/TemplateCollector';
import type {AddUnitAction} from 'containers/Units/types';


const styles = theme => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		flexDirection: 'column',
		position: 'absolute',
		top: '10%',
		left: '30%',
	},
	selectPaper: {
		display: 'flex',
		alignItems: 'center',
		width: '100%',
		padding: `${theme.spacing.unit * 2}px 0`,
	},
	select: {
	}
});

type Props = {
	classes: Object,
	parents: Array<Unit>,
	unit: Unit,
	type: string,
	addUnit: AddUnitAction,
	getDiff: Function,
}

class UnitConfiger extends React.Component<Props> {
	state = {}

	static getDerivedStateFromProps({parents}, {parent}) {
		if (!parent && parents) {
			return {
				parent: sample(parents),
			};
		}

		return null;
	}

	changeNode = (e, uid) => this.setState({
		parent: find(flow([get('uid'), isEqual(uid)]))(this.props.parents),
	});

	submit = () => {
		const {type, addUnit, getDiff} = this.props;
		const {parent: {uid}} = this.state;
		const {properties} = getDiff();

		addUnit({
			units: [{
				uid,
				units: [{type, properties}],
			}],
		});
	}

	render() {
		const {classes, type, parents, ...rest} = this.props;
		const {parent} = this.state;
		const factory = flow([
			get('factory'),
			find(flow([get('type'), isEqual(type)]))
		])(parent);

		if (!factory) {
			return null;
		}

		return (
			<div className={classes.root}>
				<Paper className={classes.selectPaper}>
					<Grid container alignItems='center' direction='column'>
						<TextField
							className={classes.select}
							value={parent.uid}
							select
							SelectProps={{native: true}}
							onChange={this.changeNode}
						>
							{
								parents.map(({uid, display_id}) => (
									<option key={uid} value={uid}>
										{display_id}
									</option>
								))
							}
						</TextField>
					</Grid>
				</Paper>
				<UnitMaker factory={factory} {...rest} />
				<Button
					className={classes.submit}
					variant='raised'
					color='primary'
					fullWidth
					onClick={this.submit}
				>
					<AddIcon />
				</Button>
			</div>
		);
	}
}

export default compose(
	withStyles(styles),
	withUnitCollector,
	withUnit,
  withTmplCollector, /* TODO: required for PropItem, examine */
)(UnitConfiger);
