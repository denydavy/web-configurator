// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
// import withStyles from '@material-ui/core/styles/withStyles';

import {withUnitSet} from 'containers/Units';
import type {UnitList} from 'containers/Units';

type Props = {
  unitSet: UnitList,
  rmUnit: Function,
};

class DeleteUnits extends React.Component<Props> {
	removeUnits = () => {
		const {rmUnit, unitSet} = this.props;

		rmUnit({units: unitSet});
	}

  render() {
    return (
      <IconButton onClick={this.removeUnits}>
        <DeleteIcon />
      </IconButton>
    );
  }
};

export default withUnitSet(DeleteUnits);
