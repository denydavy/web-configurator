// @flow

import * as React from 'react';
import {compose} from 'redux';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import type {Unit} from 'containers/Units/types';

const styles = {
	root: {
		display: 'flex',
		backgroundColor: grey[200],
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%',
		height: '100%',
	},
};

type Props = {
	unit: Unit,
	classes: Object,
};

const Storage = ({unit: {display_id, status}, classes}: Props) => (
	<div className={classes.root}>
		<Typography variant='title'>
			{display_id}
		</Typography>
		<Typography variant='subheading'>
			{status}
		</Typography>
	</div>
);

export default compose(
	withStyles(styles)
)(Storage);
