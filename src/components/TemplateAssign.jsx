// @flow

import * as React from 'react';
import {compose} from 'redux';
import map from 'lodash/fp/map';
import {FormattedMessage} from 'react-intl';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';

import {withTemplates} from 'containers/Templates';

const styles = {};

type Props = {
  selection: Array<string>,
  assignTmpl: Function,
  getTmpl: Function,
};
type State = {
  anchorElm: HTMLElement,
};

class TemplateAssign extends React.Component<Props, State> {
  state = {
    anchorElm: null,
  }

  openMenu = ({currentTarget}) => {
    this.setState({anchorElm: currentTarget});
    this.props.getTmpl();
  };

  closeMenu = () => this.setState({anchorElm: null});

  assign = tmplId => {
    const {assignTmpl, uids} = this.props;

    assignTmpl({
      templateIds: [tmplId],
      uids,
    });
  }

  //TODO: Select multiple templates to assign
  // Probably, requires dedicated class to keep selection
  render() {
    const {templates} = this.props;
    const {anchorElm} = this.state;

    return (
      <div>
        <Button variant='contained' onClick={this.openMenu}>
          <FormattedMessage id="selection.templateAssign" />
        </Button>
        <Menu
          anchorEl={anchorElm}
          open={Boolean(anchorElm)}
          onClose={this.closeMenu}
        >
          {
            templates.size
            ? <CircularProgress indeterminate />
            : map(template => (
              <MenuItem
                key={template.body.id}
                onClick={this.assign.bind(this, template.body.id)}
              >
                {template.body.name}
              </MenuItem>
            ))(templates)
          }
        </Menu>
      </div>
    );
  }
}

export default compose (
  withStyles(styles),
  withTemplates,
)(TemplateAssign);
