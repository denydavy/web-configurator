import React from 'react';
import merge from 'lodash/fp/merge';
import * as prop from 'utils/props';

// TODO: collect multiple units
export default Component => class UnitCollector extends React.Component {
	state = {
		diff: {
			properties: {},
		},
		unit: null,
	}

	static getDerivedStateFromProps({unit}, state) {
		if (unit && (!state.unit || state.unit.stripped)) {
			return {
				...state,
				unit: prop.toMap(unit),
			};
		}

		return null;
	}

	setProp = (path, item) => this.setState(({diff}) => ({
		diff: prop.set(diff, path, item),
	}));

	getDiffProp = (path, id) => prop.get(this.state.diff, path, id);
	getUnitProp = (path, id) => prop.get(this.state.unit, path, id);

	rmProp = (path, id) => this.setState(({diff}) => ({
		diff: prop.rm(diff, path, id),
	}));

	getDiff = () => prop.toList(this.state.diff);

	getUnit = () => prop.toList(
		merge(this.state.unit, this.state.diff)
	);

	render() {
		const {state, props, ...methods} = this;

		return <Component {...state} {...props} {...methods} />;
	}
}

