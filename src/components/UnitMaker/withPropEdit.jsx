// @flow

import * as React from 'react';

import type {PropertyItemProp} from './types';

const withPropEdit = Component =>
	class PropItemEdit extends React.Component<PropertyItemProp> {
		updateVal = (
			{target: {checked, value}}: 
			{target: {checked: bool, value: string | number}}
		) => {
			const {property: {id, type}, setProp, path} = this.props;

			setProp(path, {
				id,
				type,
				[`value_${type}`]: checked || value,
				properties: {},
			});
		};

		discardVal = () => {
			const {property: {id}, rmProp, path} = this.props;

			rmProp(path, id);
		};

		snapToggle = (_: Event, checked: bool) => {
			const {
				property: {id}, getUnitProp,
				setTmplProp, rmTmplProp, path
			} = this.props;

			if (checked) {
				setTmplProp(path, getUnitProp(path, id));
			} else {
				rmTmplProp(path, id);
			}
		};

		render() {
			const {props, ...methods} = this;
			return (
				<Component
					{...props}
					{...methods}
				/> 
			);
		}
	}

export default withPropEdit;
