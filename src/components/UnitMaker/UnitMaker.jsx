// @flow

import * as React from 'react';
import {compose} from 'redux';
import {FormattedMessage} from 'react-intl'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {withStyles} from '@material-ui/core/styles';

import {withWorkbench} from 'containers/Workbench';
import type {Factory, Unit} from 'containers/Units/types';

import PropertyItem from './PropertyItem';


const styles = {
	subtitle: {
		margin: '1em',
	},
};

type Props = {
	factory: Factory,
	unit: Unit,
	classes: Object,
	buildUnit: Function,
	setProp: Function,
	rmProp: Function,
}

type State = Object; 

class UnitMaker extends React.Component<Props, State> {
	// TODO: Reconsider
  // componentDidMount() {
	// 	const {unit, factory: {type}, buildUnit} = this.props;

	// 	if (!unit) {
	// 		const u = {
	// 			type
	// 		};

	// 		buildUnit(u);// How it is provided to this.props??
	// 	}
	// }

	// discardState = () => this.setState(state => {
	// 	for (const key of Object.keys(state)) {
	// 		delete state[key];
	// 	}

	// 	return state;
	// });

	// handleExpand = (e, expanded: bool) => {
	// 	if (!expanded) {
	// 		this.discardState();
	// 	}
	// }

	render() {
		const {
			classes,
			factory: {properties, type},
			...rest
		} = this.props;
		const path = [];

		return (
			<ExpansionPanel>
				<ExpansionPanelSummary>
					<Typography
						className={classes.subtitle}
						variant='title'
					>
						{type}
					</Typography>
          {/* TODO: Template selection menu */}
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<Grid key='props' container direction='column'>
						{
							properties.map(prop =>
								<PropertyItem
									key={prop.id}
									className={classes.property}
									property={prop}
									path={path}
									{...rest}
								/>
							)
						}
					</Grid>
				</ExpansionPanelDetails>
			</ExpansionPanel>
		);
	}
}

export default compose(
	withStyles(styles),
	withWorkbench,
)(UnitMaker);
