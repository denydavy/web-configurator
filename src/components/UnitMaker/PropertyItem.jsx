// @flow

import * as React from 'react';
import {compose} from 'redux';
import flatten from 'lodash/fp/flatten';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import includes from 'lodash/fp/includes';
import map from 'lodash/fp/map';
import values from 'lodash/fp/values';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';
import {withStyles} from '@material-ui/core/styles';

import BoolProperty from './BoolProperty';
import ScalarProperty from './ScalarProperty';
import withPropEdit from './withPropEdit';

import type {PropertyItemProp} from './types';

const mapOpts = map(({value_string: val}) => (
	<option key={val} value={val}>
		{val}
	</option>
));

const getConstraints = flow([
	get(['enum_constraint', 'items']),
]);

const valueType = {
	'string': 'text',
	'int32': 'number',
};

const styles = theme => ({
	root: {
		marginBottom: theme.spacing.unit,
		display: 'flex',
	},
	item: {
		flexGrow: 1,
	},
	hidden: {
		visibility: 'hidden',
	}
});

let PropertyItem;

type ItemProps = {
	updateVal: Function,
	discardVal: Function,
	...PropertyItemProp
}

class PropertyItemAccess extends React.Component<ItemProps> {
	static getPropControl = (type: string) => {
		switch (type) {
			case 'bool':
				return BoolProperty;
			case 'string':
			case 'int32':
			default:
				return ScalarProperty;
		}
	};

	render() {
		const {
			classes, property, updateVal, discardVal, snapToggle,
			path, ...rest
		} = this.props;
		const diffVal = rest.getDiffProp(path, property.id);
		const unitVal = rest.getUnitProp(path, property.id);
		const tmplVal = rest.getTmplProp(path, property.id);
		const isUnitVal = Boolean(unitVal);
		const isDiffVal = Boolean(diffVal);
		const isTmplVal = Boolean(tmplVal);
		const {
			[`value_${property.type}`]: value,
		} = diffVal || tmplVal || unitVal || {};

		const constraints = getConstraints(property);
		const hasConstraints = Boolean(constraints);
		const Item = PropertyItemAccess.getPropControl(property.type);
		const opts = mapOpts(constraints);

		return [
			<Grid className={classes.root} key={property.id}>
				<Item
					className={classes.item}
					disabled={property.readonly}
					value={value}
					onChange={updateVal}
					name={property.name}
					select={hasConstraints}
					type={valueType[property.type]}
				>
					<option value='' />
					{[...opts]}// List of constraints if available:
				</Item>
				{isDiffVal &&
					<IconButton onChange={discardVal}>
						<DeleteIcon />
					</IconButton>
				}
				{isUnitVal &&
					<Checkbox
						checked={isTmplVal}
						onChange={snapToggle}
					/>
				}
			</Grid>,
			...flow([// Append all properties provided by selected constraint
				find(flow([values, includes(value)])),
				get('properties'),
				map(prop => (
					<PropertyItem
						key={prop.id} 
						property={prop}
						path={[...path, property.id]}
						{...rest}
					/>
				)),
				flatten,
			])(constraints)
		];
	}
}

PropertyItem = compose(
	withStyles(styles),
	withPropEdit
)(PropertyItemAccess);

export default PropertyItem;
