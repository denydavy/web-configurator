// @flow

import React from 'react';
import TextField from '@material-ui/core/TextField';

import type {ItemProps} from './types';

function ScalarProperty({
	name,
	...props
}: ItemProps) {
	return (
		<TextField
			label={name}
			SelectProps={{native: true}}
			{...props}
		/>
	);
}

ScalarProperty.defaultProps = {
	value: '',
}

export default ScalarProperty;
