import type {Property} from 'containers/Units/types';

export type ItemProps = ({
	value: string | number | bool,
	select: bool,
	type: string,
	name: string,
	onChange: Function,
});

export type PropertyItemProp = {
	property: Property,
	setVal: Function,
	values: Object,
}

export type Item = {
	id: string,
	type: 'string' | 'int32' | 'bool',
	value_string?: string,
	value_int32?: number,
	value_bool?: bool,
	properties: Map<string, Item>,
}

export type Properties = {
	[string]: Item
};
