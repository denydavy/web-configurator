// @flow
import * as React from 'react';
import {compose} from 'redux';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import RemoveIcon from '@material-ui/icons/Clear';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';

import {withUnit} from 'containers/Units';
import {withWorkbench} from 'containers/Workbench';

import type {Unit} from 'containers/Units/types';

const styles = theme => ({
	r: {
		width: '100%',
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	root: {
		backgroundColor: theme.palette.background.paper,
		minWidth: 275,
		margin: theme.spacing.unit,
		opacity: 1,
		transitionDuration: theme.transitions.duration.standard,
		transitionTimingFunction: theme.transitions.easing.easeIn,
	},
	expanded: {
		opacity: 0,
		pointerEvents: 'none',
		transitionDuration: theme.transitions.duration.standard,
		transitionTimingFunction: theme.transitions.easing.easeOut,
	},
	icon: {
		color: theme.palette.primary.main,// TODO: add to theme
		// margin: theme.spacing.unit,
		// transform: 'scale(3)',
	},
	rm: {
		display: 'flex',
		flexDirection: 'column',
		backgroundColor: theme.palette.text.disabled,
		width: '100%',
	},
	title: {
		color: theme.palette.primary.contrastText,
		fontSize: 32,
		textAlign: 'center',
		position: 'absolute',
		bottom: 0,
		backgroundColor: theme.palette.text.disabled,
		width: '100%',
	}
});

type Props = {
	classes: Object,
	unit: Unit,
	setUnit: Function,
}
type State = {
	hidden: boolean,
}

const getId = uid => /DeviceIpint.(\d+)/.exec(uid)[1];

class Camera extends React.Component<Props> {
	state: State = {
		hidden: false,
		pending: false,
	}

	toggleHide = () => this.setState((state: State) => ({
		expanded: !state.expanded,
	}))

	putOnWorkbench = (e: MouseEvent) => {
		const {setUnit, unit} = this.props;

    e.stopPropagation();
		setUnit(unit.uid);
	}

	removeUnit = () => {
		const {rmUnit, unit} = this.props;

		this.setState(() => ({pending: true}));
		rmUnit({
			units: [unit],
		});
	}

	render() {
		const {classes, unit} = this.props;
		const {hidden, pending} = this.state;
		const cardClass = hidden ? classes.expanded : classes.root;

		//    return (
		//      <Card className={cardClass} onClick={this.putOnWorkbench}>
		//        <CardContent>
		//          <Typography color='textSecondary'>
		//            <VideocamIcon className={classes.icon} />
		//            {unit.display_id}
		//          </Typography>
		//        </CardContent>
		//      </Card>
		//    );
		//  }
		return (
			<div
				className={classes.r}
				style={{background: `url(https://picsum.photos/300?image=${getId(unit.uid)}`}}
			>
				{pending
          ? <CircularProgress indeterminate />
					: (
            <>
              {/*}
							<div className={classes.rm}>
								<IconButton color='secondary'>
									<RemoveIcon onClick={this.removeUnit} />
								</IconButton>
							</div>,
              {*/}
							<Typography
								className={classes.title}
								color='textSecondary'
								onClick={this.putOnWorkbench}
							>
								<VideocamIcon className={classes.icon} />
								{unit.display_id}
							</Typography>
            </>
          )
				}
			</div>
		);
	}
}

export default compose(
  withStyles(styles),
	withWorkbench,
	withUnit,
)(Camera);
