// @flow

import * as React from 'react';
import {compose} from 'redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {withStyles} from '@material-ui/core/styles';
import {withTemplates} from 'containers/Templates';

type Props = {
	templates: Array<*>,
	classes: Object,
};

const styles = {};
const TemplateList = (props: Props) => (
	<List>
		{
			props.templates.map(tmpl => (
				<ListItem>
					<ListItemText primary={tmpl.name} />
				</ListItem>
			))
		}
	</List>
);

export default compose(
	withStyles(styles),
	withTemplates,
)(TemplateList);
