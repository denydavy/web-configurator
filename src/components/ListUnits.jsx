// @flow

import * as React from 'react';
import {compose} from 'redux';
import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {unitType, withType} from 'containers/Units';
import Camera from 'components/Camera';
import Node from 'components/Node';
import Storage from 'components/Storage';
import {HexGrid, Hex} from 'components/Hex';

import type {UnitType} from 'containers/Units';
import type {UnitList} from 'containers/Units/types';
import {withSelection, SelectionType} from 'containers/Selection';
import Selection from 'components/Selection';

const listStyles = theme => ({
  item: {
    transition: theme.transitions.easing.easingInOut,
  },
  selected: {
    opacity: 1,
  },
  notselected: {
    opacity: 0.5, 
  },
});
type ListProps = {
	units: UnitList,
	Item: React.Node,
  selection: SelectionType,
  select: Function,
  deselect: Function,
};
const List = compose(
  withStyles(listStyles),
  withSelection
)(
	({units, Item, classes, selection, select, deselect}: ListProps) => 
		units.map(unit => {
      const selected = selection.has(unit.uid);
      const noSelection = selection.size === 0;
      const handleClick = selected
        ? deselect.bind(undefined, unit.uid)
        : select.bind(undefined, unit.uid); 
      const classNames = cx(classes.item, {
        [classes.selected]: noSelection || selected,
        [classes.notselected]: !(noSelection || selected),
      });

      return (
        <Hex
            className={classNames}
            key={unit.uid}
            onClick={handleClick}
        >
            <Item unit={unit} />
        </Hex>
      )
    })
);

/*
const styles = theme => ({
	root: {
		display: 'flex',
		backgroundColor: theme.palette.background.default,
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
		marginTop: theme.spacing.unit * 2,
		height: '100%',
	},
});
*/

const getComponent = (type: UnitType) => {
	switch (type) {
		case unitType.CAM:
			return Camera;
		case unitType.NODE:
			return Node;
		case unitType.STORAGE:
			return Storage;
		default:
			return 'div';
	}
};

type Props = {
	classes: Object,
	// units: UnitList,
	type: UnitType,
}
// const ListCameras = ({classes, units}: Props) => (
// 	<Grid className={classes.root}>
// 		{
// 			units.map(unit => <Camera key={unit.uid} unit={unit} />)
// 		}
// 	</Grid>
// );
const ListUnits = ({type, classes}: Props) => {
	const ListOfType = withType(type)(List);

	return (
		<HexGrid>
			<ListOfType Item={getComponent(type)} />
		</HexGrid>
	);
};

export default ListUnits; 
