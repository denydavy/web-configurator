// @flow
import * as React from 'react';
import {compose} from 'redux';
import { withStyles } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import Modal from '@material-ui/core/Modal';
import AddIcon from '@material-ui/icons/Add';
import {withUnits, withType, unitType} from 'containers/Units';
import UnitConfiger from 'components/UnitConfiger';// TODO: decide whether to generalise it
import {Hex} from 'components/Hex';

import type {UnitType, UnitList} from 'container/Units';

const MakeCam = withType(unitType.NODE)(({units}) => (
		<UnitConfiger parents={units} type={unitType.CAM} />
));
const MakeStorage = withType(unitType.NODE)(({units}) => (
		<UnitConfiger parents={units} type={unitType.STORAGE} />
));
const MakeNode = withType(unitType.DOMAIN)(({units}) => (
		<UnitConfiger parents={units} type={unitType.NODE} />
));

const styles = {
	hexPos: {
		position: 'absolute',
		left: '-10%',
		top: '8%',
		width: '113%',
	},
	hexContent: {
		background: purple[200],
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		display: 'flex',
		flexDirection: 'column',
	}
};

function getMaker(type: UnitType) {
	switch(type) {
		case unitType.NODE:
			return MakeNode;
		case unitType.CAM:
			return MakeCam;
		case unitType.STORAGE:
			return MakeStorage;
		default:
			return 'div';
	}
}

type Props = {
	type: UnitType,
	classes: Object,
	units: UnitList,
};

type State = {
	open: bool,
};

class Maker extends React.Component<Props, State> {
	state = {
		open: false,
	}

	toggleOpen = () => this.setState(({open}) => ({
		open: !open,
	}))

	render() {
		const {classes, units, type} = this.props;
		const {open} = this.state;
		const isEnabled = Boolean(units);
		const MakeUnit = getMaker(type);

		return [
			<ul key='hex' className={classes.hexPos}>
				<Hex>
					<div className={classes.hexContent} onClick={this.toggleOpen}>
						<AddIcon />
					</div>
				</Hex>
			</ul>,
			isEnabled && (
				<Modal key='dialog' open={open} onBackdropClick={this.toggleOpen}>
					<MakeUnit />
				</Modal>
			)
		];
	}
}



export default compose(
	withStyles(styles),
	withUnits,
)(Maker);
