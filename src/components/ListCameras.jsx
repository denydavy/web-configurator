// @flow
import * as React from 'react';
import {compose} from 'redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {type, withType} from 'containers/Units';
import {UnitList} from 'containers/Units/types';
import Camera from 'components/Camera';
import {HexGrid, Hex} from 'components/Hex';

const styles = theme => ({
	root: {
		display: 'flex',
		backgroundColor: theme.palette.background.default,
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
		marginTop: 16,
	},
});

type Props = {
	classes: Object,
	units: UnitList,
}
// const ListCameras = ({classes, units}: Props) => (
// 	<Grid className={classes.root}>
// 		{
// 			units.map(unit => <Camera key={unit.uid} unit={unit} />)
// 		}
// 	</Grid>
// );

const ListCameras = ({units}: Props) => (
	<HexGrid>
		{
			units.map(unit => (
				<Hex key={unit.uid}>
					<Camera unit={unit} />
				</Hex>
			))
		}
	</HexGrid>
);

export default compose(
	withType(type.CAM),
	withStyles(styles),
)(ListCameras); 
