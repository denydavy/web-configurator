import * as React from 'react';
import {compose} from 'redux';
import { FormattedMessage } from 'react-intl';
import {Link} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import MuiTabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VideocamIcon from '@material-ui/icons/Videocam';
import StorageIcon from '@material-ui/icons/Storage';
import HostIcon from '@material-ui/icons/Dvr';

export const TAB_CAMS = 'cams';
export const TAB_STORAGE = 'storage';
export const TAB_NODES = 'nodes';

const styles = theme => ({
	root: {
		display: 'flex',
		position: 'fixed',
		bottom: 0,
		pointerEvents: 'none',
		width: '100vw',
	},
	paper: {
		display: 'inline-block',
		backgroundColor: theme.palette.background.default,
		overflow: 'hidden',
		margin: 'auto',
		marginBottom: 16,
		pointerEvents: 'all',
	},
});

type Props = {
	classes: Object,
	units: UnitList,
}
const Tabs = ({classes, ...other}: Props) => (
	<div className={classes.root}>
		<Paper className={classes.paper} elevation={3}>
			<MuiTabs {...other} fullWidth>
				<Tab
					icon={<VideocamIcon />}
					label={<FormattedMessage id="common.tab.cams" />}
					value={TAB_CAMS}
				/>
				<Tab
					icon={<StorageIcon />}
					label={<FormattedMessage id="common.tab.storage" />}
					value={TAB_STORAGE}
				/>
				<Tab
					icon={<HostIcon />}
					label={<FormattedMessage id="common.tab.nodes" />}
					value={TAB_NODES}
				/>
			</MuiTabs>
		</Paper>
	</div>
);

export default compose(
	withStyles(styles),
)(Tabs); 
