// @flow
import uuidv3 from 'uuid/v3';

const CUSTOM_TEMPLATE = '0339a64a-f69e-4433-976e-4a9b7cde2e98';

export function getCustomTmplId(id: string) {
	return uuidv3(id, CUSTOM_TEMPLATE);
};
