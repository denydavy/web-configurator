// @flow
/* eslint-disable no-param-reassign, no-sequences */

type MessageRaw = {| id?: string, message?: string |};
type FilteredMessageRaw = {| id: string, message: string |};
export type ReactIntlMessages = { [messageId: string]: string };

/**
 * Tranforms messages loaded by tsv-loader to react-intl messages structure.
 */
function processMessages(
  messages: Array<FilteredMessageRaw>,
): ReactIntlMessages {
  return messages.reduce((obj, m) => ({ ...obj, [m.id]: m.message }), {});
}

/**
 * Filters messages with empty id
 */
function filterEmptyMessages(
  messages: Array<MessageRaw>,
): Array<FilteredMessageRaw> {
  return (messages.filter(m => m.id && m.message): any);
}

export default function getMessagesAsync(locale: string) {
  return import(`translations/${locale}.tsv`)
    .then(module => module.default)
    .then(filterEmptyMessages)
    .then(processMessages);
}
