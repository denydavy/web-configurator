// @flow
type _ExtractFromReturnedVal = <T>((...args: Array<any>) => T) => T;
type _ExtractFromReturnedPromise = <T>(
  (...args: Array<any>) => Promise<T>,
) => T;

export type ExtractFnReturnType<F> = $Call<_ExtractFromReturnedVal, F>;
export type ExtractFnReturnTypePromise<F> = $Call<
  _ExtractFromReturnedPromise,
  F,
>;
