// @flow
import getFp from 'lodash/fp/get';
import omit from 'lodash/fp/omit';
import setFp from 'lodash/fp/set';
import values from 'lodash/fp/values';
import reduce from 'lodash/fp/reduce';
import zip from 'lodash/fp/zip';
import times from 'lodash/fp/times';
import constant from 'lodash/fp/constant';
import flatten from 'lodash/fp/flatten';
import flow from 'lodash/fp/flow';
import initial from 'lodash/fp/initial';

import type { Unit } from 'containers/Unit/types';
import type { Item } from './types';

function completePath(path: Array<string>): Array<string> {
	return flow([
		zip(times(constant('properties'), path.length)),
		flatten,
	])(path);
}


// TODO: tests

export function set(
	unit: Unit,
	path: Array<string>,
	item: Item,
) {
	return setFp(completePath([...path, item.id]), item, unit);
}

export function rm(
	unit: Unit,
	path: Array<string>,
	id: string,
) {
	return omit(completePath([...path, id]), unit);
}

export function get(
	unit: Unit,
	path: Array<string>,
	id: string,
) {
	return getFp(completePath([...path, id]), unit);
}

export function toList(
	{properties, ...rest}:
	{properties: {[string]: Item}, rest: {[string]: mixed}}
) {
	return {
		...rest,
		properties: values(properties).map(toList),
	};
}

function indexify(coll, item) {
	return {
		...coll,
		[item.id]: toMap(item),
	};
}

export function toMap(
	{properties, ...rest}:
	{properties: {[string]: Item}, rest: {[string]: mixed}}
) {
	return {
		...rest,
		properties: reduce(indexify, {}, properties),
	};
}

const getProps = getFp('properties');

export function mergeDiff(unit, diff) {
	return {
		...unit,
		...diff,
		properties: {...getProps(unit), ...getProps(diff)},
	}
};

export default {set, get, rm, toList, toMap};
