// @flow
import * as React from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { hot } from 'react-hot-loader';
import find from 'lodash/fp/find';
import flow from 'lodash/fp/flow';
import get from 'lodash/fp/get';
import isEqual from 'lodash/fp/isEqual';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';

import Nodes, {unitType, withType} from 'containers/Units';
import Bar from 'components/Bar';
import Maker from 'components/Maker';
import Workbench from 'components/Workbench';
// import ListCameras from 'components/ListCameras';
import ListUnits from 'components/ListUnits';
import Tabs, {TAB_CAMS, TAB_STORAGE, TAB_NODES} from 'components/Tabs';

import type { Status } from './reducer';

type Props = {
  children: React.Node,
  status: Status,
  checkStatus: () => void,
};

type State = {
	tab: string,
}

const styles = {
  '@global': {
    body: {
      margin: 0,
	    position: 'relative',
    },
  },
};

const tabTypes = {
	[TAB_CAMS]: unitType.CAM,
	[TAB_STORAGE]: unitType.STORAGE,
	[TAB_NODES]: unitType.NODE,
};

//const App = ({ children, status, checkStatus }: Props) => (
class App extends React.Component<Props, State> {
	state = {
		tab: TAB_CAMS,
	}
	handleTabChange = (event, tab) => this.setState(() => ({tab}));
	render() {
		const {tab} = this.state;
		const {children} = this.props;
		const type = tabTypes[tab];

		return (
			<div>
				{/*}
				<AppBar position="static" color="default">
					<Toolbar>
						<Typography variant="title" color="inherit">
							<FormattedMessage id="common.productname" />
						</Typography>
					</Toolbar>
				</AppBar>
				{*/}
				<Bar />
				<Nodes />
				<Maker type={type} />
				<ListUnits type={type} />
				<Workbench />
				<Tabs value={tab} onChange={this.handleTabChange} />
			</div>
		);
	}
};

// const selectGlobal = ({ global: { status, tab } }) => ({ status, tab });
// const injectActions = dispatch => bindActionCreators({setTab}, dispatch);

// export const withGlobal = connect(selectGlobal, injectActions);
export default compose(
  hot(module),
  // connect(
  //   selectStatus,
  //   injectActions,
  // ),
  withStyles(styles),
)(App);
