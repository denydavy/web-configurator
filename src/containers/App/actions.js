// @flow
import {createAction} from 'redux-act';

export const loading = createAction('global/LOADING');
export const domainInit = createAction('global/DOMAIN_INIT');
export const ready = createAction('global/READY');
