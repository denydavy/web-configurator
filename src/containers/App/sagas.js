// @flow
import type { Saga } from 'redux-saga';
import { takeEvery, put } from 'redux-saga/effects';

import * as actions from './actions';

function* watcher(): Saga<void> {
//  yield takeEvery(actions.STATUS_CHECK, checkStatusSaga);
}

export default watcher;
