import {createAction} from 'redux-act';

export const initPosition = createAction('workshop/INIT_POSITION');
export const setUnit = createAction('workshop/SET_UNIT');
export const setFactory = createAction('workshop/SET_FACTORY');
