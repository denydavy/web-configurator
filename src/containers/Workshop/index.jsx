// @flow
import * as React from 'react';
import {compose} from 'redux';
import {Factory, Unit} from 'containers/Units/types';

type Props = {
	unit: Unit,
	factory: Factory,
}

class Workshop extends React.Component<Props> {

}

export default compose()(Workshop);
