// @flow

import {createAction} from 'redux-act';
import type {
	GetTmplAction, SetTmplAction, ListTmplAction, AssignTmplAction
} from './types'

export const getTmpl: GetTmplAction = createAction('template/REQUEST');
export const addTmpl: SetTmplAction = createAction('template/ADD');
export const modTmpl: SetTmplAction = createAction('template/MODIFY');
export const rmTmpl: SetTmplAction = createAction('template/REMOVE');
export const listTmpl: ListTmplAction = createAction('template/LIST');
export const assignTmpl: AssignTmplAction = createAction('template/ASSIGN');
