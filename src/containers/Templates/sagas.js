//@ flow

import {takeEvery, put, call, select} from 'redux-saga/effects';

import logger from 'utils/logger';
import * as api from 'api/templates';
import * as actions from './actions';

import type {Saga} from 'redux-saga';


function* getTemplates() {
	const {items, error} = yield call(api.getTemplates);

	if (error) {
		logger.error('Templates list failed to fetch');
	}
	
  if (items) {
    yield put(actions.listTmpl(items));
  }
}

function* updateTemplates(changeset) {
	const {changes, error} = yield call(api.postTmplChanges, changeset);

	if (error) {
		logger.error('Template change failed');
	}
	logger.debug(changes);
}

function* addTemplates({payload: template}) {
	yield* updateTemplates({
		created: [template],
		modified: [],
		removed: [],
	});
}

function* modifyTemplates({payload: template}) {
	yield* updateTemplates({
		created: {items: []},
		modified: {items:[template]},
		removed: {items: []},
	});
}

function* rmTemplates(tmplIds) {
	yield* updateTemplates({
		created: {items: []},
		modified: {items: []},
		removed: tmplIds,
	});
}

function* assignTemplates({payload: {uids, templateIds}}) {
	const changeset = {
    items: uids.map((uid: string) => ({
			unit_id: uid,
			template_ids: templateIds,
		})),
	};

	const {error} = yield call(api.postTmplAssign, changeset);

	if (error) {
		logger.error('Templates change failed');
	}
}

function* sagas(): Saga<void> {
	yield takeEvery(actions.getTmpl, getTemplates);
	yield takeEvery(actions.addTmpl, addTemplates);
	yield takeEvery(actions.modTmpl, modifyTemplates);
	yield takeEvery(actions.rmTmpl, rmTemplates);
	yield takeEvery(actions.assignTmpl, assignTemplates);
}

export default sagas;
