import {compose, bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from './actions';

const mapActionsToProps = dispatch => bindActionCreators(actions, dispatch);
export const withSelection =
	connect(({selection}) => ({selection}), mapActionsToProps);
export type {SelectionType} from 'reducer';
