import {createAction} from 'redux-act';

export const select = createAction('selection/SELECT');
export const deselect = createAction('selection/DESELECT');
export const clear = createAction('selection/CLEAR');

