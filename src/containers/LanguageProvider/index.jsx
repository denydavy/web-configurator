// @flow
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import locale from 'translations';
import getMessagesAsync, { type ReactIntlMessages } from 'utils/i18n';

type Props = {
  children: React.Node,
};

type State = {
  messages: ReactIntlMessages | null,
  error: boolean,
};

class LanguageProvider extends React.Component<Props, State> {
  state = {
    messages: null,
    error: false,
  };

  componentWillMount() {
    getMessagesAsync(locale)
      .then(this.updateMessages)
      .catch(this.handleError);
  }

  handleError = () => this.setState({ error: true });

  updateMessages = (messages: ReactIntlMessages) => this.setState({ messages });

  render() {
    const { children } = this.props;
    const { messages, error } = this.state;

    if (error) {
      return 'Localization error';
    }
    if (!messages) {
      return null;
    }

    return (
      <IntlProvider locale={locale} messages={messages} key={locale}>
        {React.Children.only(children)}
      </IntlProvider>
    );
  }
}

export default LanguageProvider;
