// @flow
import {takeEvery, put, call, select} from 'redux-saga/effects';
import get from 'lodash/fp/get';
import isEqual from 'lodash/fp/isEqual';
import flow from 'lodash/fp/flow';
import map from 'lodash/fp/map';
import set from 'lodash/fp/set';
import size from 'lodash/fp/size';
import uniq from 'lodash/fp/uniq';
import * as api from 'api/units';
import logger from 'utils/logger';

import * as appActions from 'containers/App/actions';
import * as actions from './actions';
import {getStrippedNodes, unitBfs} from './utils';

import type {Saga} from 'redux-saga';
import type {Unit} from './types';

const NO_PARENT = {parent: null};
const isRoot = flow([get(['units', '0', 'type']), isEqual('root')]);
const hasNoChild = flow([get(['units', '0', 'units']), size, isEqual(0)]); 
const isRootEmpty = units => isRoot(units) && hasNoChild(units);

function* applyUnitChangeset(changeset): Saga<void> {
	const {changes, error}: {changes: string, error: string} =
		yield call(api.postChanges, changeset);

	if (error) {
		logger.error('Unit change failed');
	}
	logger.debug(changes);
}

function* initDomain({units: [{uid, factory: [domain]}]}): Saga<void> {
	const setDomainName = set(['properties', '0', 'value_string'], 'domain')
	const added = [{
		uid,
		units: [
			setDomainName(domain),
		],
	}];

	yield put(appActions.domainInit());

	yield* applyUnitChangeset({added});
}

function* getUnits({payload}): Saga<Unit> {
	logger.debug(payload);
	yield put(appActions.loading());
	const {units, error} = yield call(api.fetchUnits, payload);

	logger.debug('Unit %o', units);

	if (error) {
		logger.error('Unit fetch failed');
	}
	if (units.not_found_objects) {
		logger.log(units.not_found_objects);
	}
	if (units.unreachable_objects) {
		logger.log(units.unreachable_objects);
	}

	yield put(appActions.ready());
	yield put(actions.updateUnit(units));
	return units;
}

function* getServices(): Saga<void> {
	let units = yield* getUnits(actions.fetchUnit(NO_PARENT));

	if (units && isRootEmpty(units)) {
		logger.debug('Domain is not initialised');
		yield* initDomain(units);
		units = yield* getUnits(actions.fetchUnit(NO_PARENT));

	}

	yield put(actions.updateUnit(units));

	const strippedNodes = getStrippedNodes(units);

	logger.debug('Requesting Nodes: %o', strippedNodes);
	units = yield* getUnits(actions.fetchUnit({parent: strippedNodes}));

	if (units) {
		yield put(actions.updateUnit(units));
	}

};

const selectUnits = ({units}) => (units);
function* handleUnitRequest({payload: parent}): Saga<void> {
	const units = yield select(selectUnits);
	const path = unitBfs(units, parent);
	const unit = get(path)(units);
	
	if (unit && !unit.stripped) {
		return;
	}
	yield put(actions.fetchUnit({parent}));
}

const getUniqueUids = flow([
	map(get('uid')),
	uniq,
]);
const parseParent = uid => uid.replace(/\/[^/]*$/, '');

function* getUnitUpdates(parents) {
	for (const parent of parents) {
		yield* getUnits(actions.requestUnit({parent}));
	}
}

function* addUnit({payload: {units}}): Saga<void> {
	yield* applyUnitChangeset({added: units});
	yield* getUnitUpdates(getUniqueUids(units));
}

function* rmUnit({payload: {units}}): Saga<void> {
	yield* applyUnitChangeset({removed: units});
	yield* getUnitUpdates(map(parseParent, getUniqueUids(units)));
}

function* changeUnit({payload: {units}}): Saga<void> {
	yield* applyUnitChangeset({changed: units});
	yield* getUnitUpdates(map(parseParent, getUniqueUids(units)));
}

function* watcher(): Saga<void> {
	yield takeEvery(actions.getServices, getServices);
	yield takeEvery(actions.fetchUnit, getUnits);
	yield takeEvery(actions.requestUnit, handleUnitRequest);
	yield takeEvery(actions.changeUnit, changeUnit);
	yield takeEvery(actions.addUnit, addUnit);
	yield takeEvery(actions.rmUnit, rmUnit);
}

export default watcher;
