// @flow
import {createReducer} from 'redux-act';
import assign from 'lodash/fp/assign';
import get from 'lodash/fp/get';
import reduce from 'lodash/fp/reduce';
import update from 'lodash/fp/update';
import set from 'lodash/fp/set';
import {unitBfs} from './utils';
import {updateUnit} from './actions';
import type {Unit, UnitUpdatePayload} from './types';

const initialState: Unit =  {
	units: [{type: 'root', uid: 'root'}],
};

export const getParent = get('uid');

export default createReducer({
		[updateUnit]: (
			state: Unit,
			{units}: UnitUpdatePayload
		) => reduce((state_, unit) => {
			const path = unitBfs(state_, getParent(unit));

			return update(path, () => unit)(state_);
		}, state, units),
	},
	initialState
);
