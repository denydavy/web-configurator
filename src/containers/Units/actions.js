// @flow
import {createAction} from 'redux-act';
import type {
	RequestUnitAction, FetchUnitAction, ChangeUnitAction, UpdateUnitAction,
	AddUnitAction, RmUnitAction,
} from './types';

export const requestUnit: RequestUnitAction =
	createAction('unit/REQUEST');
export const changeUnit: ChangeUnitAction =
	createAction('unit/CHANGE');
export const updateUnit: UpdateUnitAction =
	createAction('unit/UPDATE');
export const fetchUnit: FetchUnitAction =
	createAction('unit/FETCH');
export const addUnit: AddUnitAction =
	createAction('unit/ADD');
export const rmUnit: RmUnitAction =
	createAction('unit/REMOVE');

export const getServices = createAction('cfg/GET_SERVICES');
