// @flow
import * as React from 'react';
import {compose, bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
  selectNodes, selectSvc, selectType, selectUid,
  getUnitsByUid,
} from './utils';
import * as actions from './actions';
import * as unitType from './constants';
import type {UnitList, Uid} from './types';
// TODO: Learn how to import all types


type Props = {
  getServices: Function,
};

class Nodes extends React.Component<Props> {
	componentDidMount() {
		const {getServices} = this.props;

		getServices();
	}

	render() {
		return null
	}
}

export type UnitType = $Keys<typeof unitType>;
export type {UnitList};
export {unitType};

const mapActionsToProps = dispatch => bindActionCreators(actions, dispatch);

export const withNodes =
	connect(({units}) => ({nodes: selectNodes(units)}), mapActionsToProps);
export const withSvc = 
	connect(({units}) => ({services: selectSvc(units)}), mapActionsToProps);
export const withType = (type: unitType) =>
	connect(({units}) => ({units: selectType(type)(units)}), mapActionsToProps);

const selectUnit = Component => (
	props: {units: UnitList, uid: Uid}
) => <Component unit={selectUid(props.uid, props.units)} {...props} />

export const withUnits = connect(({units}) => ({units}), mapActionsToProps);
export const withUnit = compose(
  withUnits,
  selectUnit,
);
const selectUnitSet = Component => (
	props: {units: UnitList, uids: Array<Uid>}
) => <Component unitSet={getUnitsByUid(props.uids, props.units)} {...props} />
export const withUnitSet = compose(
  withUnits,
  selectUnitSet,
);
/*
export const ProvideUnits = withUnits((
	props: {units: UnitList, uids: Array<Uid>, children: React.Node}
) => (
  <>
  {
    React.Children.map(
      props.children,
      child => (
        React.cloneElement(child, {
          units: getUnitsByUid(props.uid, props.units),
          ...props
        })
      )
    )
  }
  </>
));
*/
export const getUnitByUid = selectUid;
export default withNodes(Nodes);
