// @flow

import {CAM, STORAGE, DETECTOR} from './constants';

export type Uid = string;
export type Constraint = {
	properties: Property,// eslint-disable-line
	value_string: string,
};

export type EnumConstraint = {
	items: Array<Constraint>,
};

export type Property = {
	id: string,
	name: string,
	type: 'string' | 'int32' | 'bool',
	value_string?: string,
	value_int32?: number,
	value_bool?: bool,
	enum_constraint?: EnumConstraint,
};

export type Factory = {
	type: string,
	properties: Array<Property>,
};

export type Unit = {
	type: string,
	display_id: string,
	uid: Uid,
	status?: string,
	stripped?: bool,
	factory?: Array<Factory>,
	units?: Array<Unit>,
};

export type UnitType = CAM | STORAGE | DETECTOR;

export type Changeset = {
	added?: Array<Unit>,
	changed?: Array<Unit>,
	removed?: Array<Unit>,
};

export type Changes = {
	added: Array<Uid>,
	changed: Array<Uid>,
	removed: Array<Uid>,
}

export type UnitList = Array<Unit|empty>;

/** Action Types */
export type RequestParams = {
	parent: Array<string> | string | void,
};

export type UpdateParams = {
	units: UnitList,
};

export type ChangeParams = {
	changeset: Changeset,
};

export type RmParams = {
	parent: string,
	units: UnitList,
};

export type UnitRequestPayload = {
	parent: string,
};

export type UnitChangedPayload = {
	changeset: Changeset,
};

export type UnitUpdatePayload = {
	units: UnitList,
};

export type RequestUnitAction = { 
	(RequestParams): {type: string, payload: RequestParams},
	toString: string,
};

type ToString = () => string;
export type FetchUnitAction = {
	(RequestParams): {type: string, payload: RequestParams},
	toString: ToString,
};

export type ChangeUnitAction = {
	(ChangeParams): {type: string, payload: UpdateParams},
	toString: ToString,
};

export type UpdateUnitAction = {
	(UpdateParams): {type: string, payload: UpdateParams},
	toString: ToString,
};

export type AddUnitAction = {
	(UpdateParams): {type: string, payload: UpdateParams},
	toString: ToString,
};

export type RmUnitAction = {
	(RmParams): {type: string, payload: UpdateParams},
	toString: ToString,
};
