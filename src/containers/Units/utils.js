// @flow

import curry from 'lodash/fp/curry';
import flow from 'lodash/fp/flow';
import fpMap from 'lodash/fp/map';
import flatten from 'lodash/fp/flatten';
import flatMap from 'lodash/fp/flatMap';
import filter from 'lodash/fp/filter';
import get from 'lodash/fp/get';
import negate from 'lodash/fp/negate';
import isNil from 'lodash/fp/isNil';
import isEqual from 'lodash/fp/isEqual';

import type {Uid, Unit, UnitList} from './types';

type Wrapper = {
	path: Array<number>,
	unit: Unit,
}
const map = fpMap.convert({cap: false});
const getUnits = get('units');
const getUnitId = get(['unit', 'uid']);
const levelDown = flow(
	fpMap(({unit: root, path}) =>
		map((unit, idx) => 
			({unit, path: [...path, 'units', idx]})
		)(getUnits(root))
	),
	flatten,
	filter(negate(isNil)),
);
export const unitBfs = (unit: Unit, searchUid: string): Array<string> | void => {
	let items:Array<Wrapper> = [{path: [], unit}];
	let idx: number;

	do {
		items = levelDown(items);
		idx = items.findIndex(flow([getUnitId, isEqual(searchUid)]));
	} while (idx === -1 && items.length > 0)

	return get([idx, 'path'])(items);
};
export function flatUnits({units, ...unit}, list = []) {
    list.push(unit);
    if (units) {
        units.forEach(u => flatUnits(u, list));
    }

    return list;
}
export const selectNodes = flow([
	getUnits,// root
	flatMap(getUnits),// domains
	flatMap(getUnits),// nodes
]);
export const selectSvc = flow([
	selectNodes,
	flatMap(getUnits),// services
]);
export const selectType = type => flow([
	flatUnits,
	filter(flow(get('type'), isEqual(type)))
]);
export const selectUid = curry((uid: Uid, state) => 
	get(unitBfs(state, uid), state));
export const getUnitsByUid = curry((uids: Array<Uid>, units: UnitList) => {
	const uidSet = new Set(uids);

	return flow([
		flatUnits,
		filter(({uid}: Unit) => uidSet.has(uid))
	])(units);
})
export const getStrippedNodes = flow([
	selectNodes,
	filter(negate(isNil)),
	map(node => node.stripped && node.uid),
]);
