// @flow
import {createReducer} from 'redux-act';
import {Record, Set} from 'immutable';
import filter from 'lodash/fp/filter';
import set from 'lodash/fp/set';
import omit from 'lodash/fp/omit';
import * as actions from './actions';

import type {RecordFactory, RecordOf} from 'immutable';

type StateType = {
	uid: string,
	extensions: Set<string>,
	units: Array<Object>,
};
const State: RecordFactory<StateType> = Record({
	uid: '',
	extensions: Set(),
	units: [],
});
const initialState = State();
const updateUnit = (op: Function, unit: Object) =>
	(units: Array<Object>): Array<Object> => {

	const idx: number = units.indexOf(unit);

	op(units[idx]);

	return units;
}

export default createReducer({
	[actions.setUnit]: (state: RecordOf<State>, uid: string) => state.set('uid', uid),
	[actions.addExtUnit]: (state: RecordOf<State>, uid: string) => 
		state.set('extensions', state.get('extensions').set(uid)),
	[actions.rmExtUnit]: (state: RecordOf<State>, uid: string) =>
		state.set('extensions', state.get('extensions').delete(uid)),
	[actions.buildUnit]: (state: RecordOf<State>, unit) =>
		state.set('units', unit),
	[actions.addUnit]: (state: RecordOf<State>, unit) =>
		state.update('units', units => [unit, ...units]),
	[actions.rmUnit]: (state: RecordOf<State>, unit) =>
		state.update('units', filter(u => u !== unit)),
	[actions.setUnitProp]: (state: RecordOf<State>, [unit, propname, propval]) =>
		state.update('units', updateUnit(set(propname, propval), unit)),
	[actions.rmUnitProp]: (state: RecordOf<State>, [unit, propname]) => 
		state.update('units', updateUnit(omit(propname), unit)),
}, initialState);
