type CommonAction<T> = {
	type: string,
	payload: T,
}

type ToString = () => string;

export type SetUnitAction = {
	(string): CommonAction<string>,
	toString: ToString,
}
export type AddExtUnitAction = {
	(string): CommonAction<string>,
	toString: ToString,
}
export type RmExtUnitAction = {
	(string): CommonAction<string>,
	toString: ToString,
}

export type BuildUnitAction = {
	(Object): CommonAction<Object>,
	toString: ToString,
}

export type PropVal = [string, (number | string | bool)];
export type SetUnitPropAction = {
	(PropVal): CommonAction<PropVal>,
	toString: ToString,
}

export type RmUnitPropAction = {
	(string): CommonAction<string>,
	toString: ToString,
}

