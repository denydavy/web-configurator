// @flow
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from './actions';

const mapStateToProps = ({workbench}) => ({
	uid: workbench.get('uid'),
	extensions: workbench.get('extensions'),
	units: workbench.get('units'),
});
const mapActionsToProps = dispatch => bindActionCreators(actions, dispatch);

export const withWorkbench = connect(mapStateToProps, mapActionsToProps);
export default {withWorkbench};
